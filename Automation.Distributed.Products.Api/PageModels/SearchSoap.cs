﻿using Automation.FluentApi.Internal;
using System.Net.Http;
using System.Text;

namespace Automation.Distributed.Products.Api.PageModels
{
    public abstract class SearchSoap : ApiFactory
    {
        //! MEMBERS: api
        public const string m_Envelope = "http://schemas.xmlsoap.org/soap/envelope/";
        public const string m_Authentication = "http://schemas.tourico.com/webservices/authentication";

        //! MEMBERS: state
        public string m_Destination, m_DateFormat, m_UserName, m_Password, m_Culture, m_Version;
        public int m_From, m_To;

        public virtual string GenerateRequestXml()
        {
            return string.Empty;
        }

        public virtual HttpResponseMessage PostRequest()
        {
            HttpClient.DefaultRequestHeaders.Add("SOAPAction", Repository["SoapAction"]);
            //
            //-- generate request body
            var stringContent = new StringContent(GenerateRequestXml(), Encoding.UTF8, "text/xml");
            //
            //-- post request
            return HttpClient.PostAsync(Application, stringContent).GetAwaiter().GetResult();
        }
    }
}