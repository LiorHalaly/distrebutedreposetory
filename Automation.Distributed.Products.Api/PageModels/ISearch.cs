﻿using Automation.Distributed.Products.Api.Internal.Actions;
using Automation.Distributed.Products.Api.Internal.Properties;
using Automation.FluentApi.Internal;

namespace Automation.Distributed.Products.Api.PageModels
{
    public interface ISearch<T> :
        IApiFactory,
        IPropDestination<T, string>,
        IPropFrom<T, int>,
        IPropTo<T, int>,
        IPropDateFormat<T, string>,
        IPropUserName<T, string>,
        IPropPassword<T, string>,
        IPropVersion<T, string>,
        IPerform<ISearchResults<object>>
    {
    }
}