﻿using Automation.FluentApi.Internal;

namespace Automation.Distributed.Products.Api.PageModels
{
    public interface ISearchResults<T> : IApiFactory
    {
        T Entities();
    }
}