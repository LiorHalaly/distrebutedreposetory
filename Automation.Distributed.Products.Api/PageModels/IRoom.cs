﻿using Automation.Distributed.Products.Api.Internal.Properties;
using Automation.FluentApi.Internal;
using System.Collections.Generic;

namespace Automation.Distributed.Products.Api.PageModels
{
    public interface IRoom :
        IApiFactory,
        IPropNumberOfAdults<IRoom, int>,
        IPropNumberOfChildren<IRoom, int>,
        IPropAges<IRoom, IEnumerable<int>>
    {
    }
}