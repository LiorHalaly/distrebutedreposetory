﻿using Automation.FluentApi.Internal;
using Gravity.Drivers.Selenium;
using OpenQA.Selenium;
using System;

namespace Automation.Distributed.Products.Api.PageModels
{
    public abstract class SearchUi : ApiFactory
    {
        //! MEMBERS: state
        public string m_Destination, m_DateFormat, m_UserName, m_Password;
        public int m_From, m_To;

        //public  SearchUi()
        //{
        //    //-- set defaults
        //    m_Destination = "NYC";
        //    m_DateFormat = "yyyy-MM-dd";
        //    m_UserName = "tgsautomation";
        //    m_Password = "Pa$$word1";
        //    m_From = 30;
        //    m_To = 32;
        //}

        public virtual ISearchResults<object> Perform()
        {
            //! pipe: step #0
            StartBannersListener();

            //! pipe: step #1
            Login();

            //! pipe: step #2
            SetDestination();

            //! pipe: step #3
            SelectFromAutoComplete();

            //! pipe: step #4
            DatePickerPipeline();

            //! pipe: step #5
            ClickOnSearchButton();
            //
            //-- return new search results object
            //return new SearchResultsHotelsUi();
            return null;
        }

        //! perform pipeline
        internal void Login()
        {
            //-- exit conditions
            if (!(m_UserName.Length > 0 && m_Password.Length > 0)) return;
            //
            //-- set user-name
            WebDriver.GetElement(By.XPath(Repository["UserName"])).SendKeys(m_UserName);
            Logger.Info($"type [{m_UserName}] into [user-name] text-box");
            //
            //-- set password
            WebDriver.GetElement(By.XPath(Repository["Password"])).SendKeys(m_Password);
            Logger.Info($"type [{m_Password}] into [password] text-box");
            //
            //-- click on login button
            WebDriver.GetElement(By.XPath(Repository["Login"])).Click();
            Logger.Info("click on [login] button");
        }

        internal void StartBannersListener()
        {
            WebDriver.BannersListener(By.XPath(Repository["Banners"]));
        }

        internal void SetDestination()
        {
            //-- get destination text-box and send keys to it
            WebDriver.GetElement(By.XPath(Repository["Destination"])).SendKeys(m_Destination);
            Logger.Info($"type {m_Destination} into [destination] text-box under [search] panel");
        }

        internal void SelectFromAutoComplete()
        {
            //-- select the first auto-complete item
            WebDriver.GetDisplayedElement(By.XPath(Repository["AutoComplete"])).Click();
            Logger.Info("click on the first list-item under [auto-complete] panel");
        }

        internal void ClickOnSearchButton()
        {
            WebDriver.GetElement(By.XPath(Repository["SearchButton"])).Click();
            Logger.Info("click on [search] button under [search] panel");
        }

        //! perform pipeline: date-picker pipeline
        internal void DatePickerPipeline()
        {
            //-- pipe: step #1
            OpenDatePicker();

            //-- pipe: step #2
            PrepareFromDate();

            //-- pipe: step #3
            PickDay(m_From);

            //-- pipe: step #4
            PrepareToDate();

            //-- pipe: step #5
            PickDay(m_To);
        }

        private void OpenDatePicker()
        {
            //-- activate date-picker
            WebDriver.GetElement(By.XPath(Repository["DatePickerOpen"])).Click();
            //
            //-- log/debug entry
            Logger.Info($"click on [{Repository["DatePickerOpen"]}] element under [search] panel");
        }

        private void PrepareFromDate()
        {
            //-- get the number of months to iterate
            var deltaDays = DateTime.Now.AddDays(m_From).Subtract(DateTime.Now).Days;
            var numberOfMonths = deltaDays / 30;
            //
            //-- iterate months
            MoveToNextMonth(numberOfMonths);
        }

        private void PrepareToDate()
        {
            //-- get to & from 'day'
            var toDay = DateTime.Now.AddDays(m_To).Day;
            var fromDay = DateTime.Now.AddDays(m_From).Day;
            //
            //-- get to & from 'month'
            var toMonth = DateTime.Now.AddDays(m_To).Month;
            var fromMonth = DateTime.Now.AddDays(m_From).Month;
            //
            //-- get to & from 'month'
            var toYear = DateTime.Now.AddDays(m_To).Year;
            var fromYear = DateTime.Now.AddDays(m_From).Year;
            //
            //-- check if another 'next' action is required (will allow negative test for invalid check-in/check-out)
            if (!(toDay < fromDay && (toMonth > fromMonth || toYear > fromYear))) return;
            //
            //-- normalize date-picker state
            var deltaMonths = ((toMonth - fromMonth) < 0) ? toYear - fromYear : toMonth - fromMonth;
            MoveToNextMonth(deltaMonths);
        }

        private void PickDay(int numberOfDaysFromToday)
        {
            //-- get the day to pick
            var day = DateTime.Now.AddDays(numberOfDaysFromToday).Day;
            //
            //-- pick the relevant day
            WebDriver
                .WaitForDisplayedElement(By
                .XPath(string.Format(Repository["DatePickerDay"], day)))
                .Click();
            Logger.Info($"click on [{day}] button under [select-date] date-picker");
        }

        private void MoveToNextMonth(int numberOfMonths)
        {
            //-- exit conditions
            if (numberOfMonths == 0) return;
            //
            //-- iterate months
            for (int i = 0; i < numberOfMonths; i++)
            {
                WebDriver
                    .WaitForDisplayedElement(By
                    .XPath(Repository["DatePickerNext"]))
                    .Click();
            }
            Logger.Info($"click on [next-month] button under [select-date] date-picker {numberOfMonths} time(s)");
        }
    }
}