﻿using Automation.Distributed.Products.Api.Internal.Properties;
using Automation.FluentApi.Internal;

namespace Automation.Distributed.Products.Api.PageModels.Hotels
{
    public interface ISearchHotels :
        IApiFactory,
        ISearch<ISearchHotels>,
        IPropMaxPrice<ISearchHotels, decimal>,
        IPropAvailableOnly<ISearchHotels, bool>,
        IPropExactDestination<ISearchHotels, bool>,
        IPropPropertyType<ISearchHotels, string>,
        IPropStarLevel<ISearchHotels, int>
    {
        ISearchHotels AddRoom(IRoom room);
    }
}
