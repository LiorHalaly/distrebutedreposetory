﻿namespace Automation.Distributed.Products.Api.Internal.Actions
{
    public interface IPerform<T>
    {
        T Perform();
    }
}
