﻿namespace Automation.Distributed.Products.Api.Internal.Properties
{
    public interface IPropDateFormat<T, U>
    {
        U DateFormat();
        T DateFormat(U dateFormat);
    }
}
