﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Distributed.Products.Api.Internal.Properties
{
    public interface IPropMaxPrice<T,U>
    {
        U MaxPrice();
        T MaxPrice(U maxPrice);
    }
}
