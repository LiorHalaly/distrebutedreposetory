﻿namespace Automation.Distributed.Products.Api.Internal.Properties
{
    public interface IPropDestination<T, U>
    {
        U Destination();
        T Destination(U destination);
    }
}
