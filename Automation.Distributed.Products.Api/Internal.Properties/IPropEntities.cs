﻿namespace Automation.Distributed.Products.Api.Internal.Properties
{
    public interface IPropEntities
    {
        T Entities<T>();
    }
}
