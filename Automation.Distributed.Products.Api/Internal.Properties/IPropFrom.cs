﻿namespace Automation.Distributed.Products.Api.Internal.Properties
{
    public interface IPropFrom<T, U>
    {
        U From();
        T From(U from);
    }
}
