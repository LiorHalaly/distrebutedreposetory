﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Distributed.Products.Api.Internal.Properties
{
    public interface IPropNumberOfChildren<T, U>
    {
        U NumberOfChildren();
        T NumberOfChildren(U numberOfChildren);
    }
}
