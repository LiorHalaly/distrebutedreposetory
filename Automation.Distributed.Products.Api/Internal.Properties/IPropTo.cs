﻿namespace Automation.Distributed.Products.Api.Internal.Properties
{
    public interface IPropTo<T, U>
    {
        U To();
        T To(U to);
    }
}
