﻿using System.Text;

namespace Automation.FluentApi.Internal.Resources
{
    public static class SqlRepository
    {
        public static string CreateDatabase()
        {
            //-- create script writer
            var sqlWritter = new StringBuilder();
            //
            //-- create default automation database
            sqlWritter.Append("USE [master] ");
            sqlWritter.Append("IF NOT EXISTS (SELECT name FROM master.sys.databases WHERE name = N'Automation') ");
            sqlWritter.Append("BEGIN ");
            sqlWritter.Append("	   CREATE DATABASE [Automation] ");
            sqlWritter.Append("END;");
            //
            //-- return script
            return sqlWritter.ToString();
        }

        public static string CreateSchema()
        {
            var sqlWritter = new StringBuilder();

            //-- ENVIRONMENTS -------------------------------------------------------
            //
            // Primary automation table. Holds the information about the
            // automation available environments and running status.
            // This table is linked to all test data entities, with ID field.
            //-----------------------------------------------------------------------
            sqlWritter.Append("USE [Automation]");
            sqlWritter.Append("IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = N'ENVIRONMENTS')");
            sqlWritter.Append("BEGIN ");
            sqlWritter.Append("CREATE TABLE [dbo].[ENVIRONMENTS] ");
            sqlWritter.Append("	     ([ID]                 [int] IDENTITY(1,1) NOT NULL");
            sqlWritter.Append("	     ,[SITE_TYPE]          [nvarchar](max)     NULL");
            sqlWritter.Append("	     ,[APPLICATION_PATH]   [nvarchar](max)     NULL");
            sqlWritter.Append("	     ,[SITE_ENV]           [nvarchar](max)     NULL");
            sqlWritter.Append("	     ,[FOR_AUTOMATION]     [int]               NOT NULL");
            sqlWritter.Append("	     ,[ENV_TIMEOUT]        [nvarchar](15)      NULL");
            sqlWritter.Append("	     ,[LOGIN_PARAMS]       [nvarchar](max)     NULL");
            sqlWritter.Append("      ,[ENVIRONMENT_LEVEL]  [nvarchar](max)     NULL");
            sqlWritter.Append("	     ,[RETRIES]            [int]               NOT NULL");
            sqlWritter.Append("   	 ,CONSTRAINT [PK_ENVIRONMENTS] PRIMARY KEY CLUSTERED ([ID] ASC) ");
            sqlWritter.Append("  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ");
            sqlWritter.Append("    ON [PRIMARY]) ");
            sqlWritter.Append("	   ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]; ");

            sqlWritter.Append("INSERT INTO [dbo].[ENVIRONMENTS] ");
            sqlWritter.Append("    ([SITE_TYPE],[APPLICATION_PATH],[SITE_ENV],[FOR_AUTOMATION],[ENV_TIMEOUT],[LOGIN_PARAMS],[RETRIES]) ");
            sqlWritter.Append("VALUES");
            sqlWritter.Append("	   ('Fake', 'http://www.fakesiteurl.fk.fk','Test',1,'60000',NULL,1) ");
            sqlWritter.Append("END;");

            //-- TEST_LEVEL ---------------------------------------------------------
            //
            // Hold the test level which determinants which iteration of which test
            // will be activated (bitwise engine).
            // The level values must be in bits values (i.e. 2, 4, 8, 16, 32, etc.)
            //-----------------------------------------------------------------------
            sqlWritter.Append("IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = N'TEST_LEVEL') ");
            sqlWritter.Append("BEGIN ");
            sqlWritter.Append("CREATE TABLE [dbo].[TEST_LEVEL] ");
            sqlWritter.Append("      ([ID]                     [int] IDENTITY(1,1) NOT NULL");
            sqlWritter.Append("	     ,[TEST_LEVEL_DESCRIPTION] [nvarchar](max)     NULL");
            sqlWritter.Append("	     ,[TEST_LEVEL_VALUE]       [int]               NOT NULL");
            sqlWritter.Append("	     ,[FOR_AUTOMATION]         [int]               NULL");
            sqlWritter.Append("	     ,CONSTRAINT [PK_TEST_LEVEL] PRIMARY KEY CLUSTERED ([ID] ASC) ");
            sqlWritter.Append("  WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ");
            sqlWritter.Append("    ON [PRIMARY]) ");
            sqlWritter.Append("	   ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];");

            sqlWritter.Append("INSERT INTO [dbo].[TEST_LEVEL] ");
            sqlWritter.Append("	   ([TEST_LEVEL_DESCRIPTION],[TEST_LEVEL_VALUE],[FOR_AUTOMATION]) ");
            sqlWritter.Append("VALUES ");
            sqlWritter.Append("	   ('Full System Test',2,1)");
            sqlWritter.Append("END;");

            return sqlWritter.ToString();
        }

        public static string CreateTable()
        {
            //-- create script writer
            var sqlWritter = new StringBuilder();
            //
            //-- create base automation schema-script
            sqlWritter.Append("USE [Automation]");
            sqlWritter.Append("IF NOT EXISTS (SELECT name FROM sys.tables WHERE name = N'{0}') ");
            sqlWritter.Append("      BEGIN ");
            sqlWritter.Append("CREATE TABLE [dbo].[{0}](");
            sqlWritter.Append("			    [ITERATION_ID]        [int] IDENTITY(1,1) NOT NULL");
            sqlWritter.Append("			   ,[APPLICATION_PATH_ID] [int]               NOT NULL");
            sqlWritter.Append("		       ,[FOR_AUTOMATION]      [int]               NOT NULL");
            sqlWritter.Append("			   ,[TEST_LEVEL]          [nvarchar](MAX)     NOT NULL");
            sqlWritter.Append("			   ,{1} ");
            sqlWritter.Append("  CONSTRAINT [PK_{0}] PRIMARY KEY CLUSTERED ([ITERATION_ID] ASC)");
            sqlWritter.Append("        WITH (PAD_INDEX = OFF");
            sqlWritter.Append("	            ,STATISTICS_NORECOMPUTE = OFF");
            sqlWritter.Append("	            ,IGNORE_DUP_KEY         = OFF");
            sqlWritter.Append("	            ,ALLOW_ROW_LOCKS        = ON");
            sqlWritter.Append("	            ,ALLOW_PAGE_LOCKS       = ON) ON [PRIMARY])");
            sqlWritter.Append("	          ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];");

            sqlWritter.Append("ALTER TABLE [dbo].[{0}]");
            sqlWritter.Append("       WITH CHECK ADD CONSTRAINT [FK_{0}_ENVIRONMENTS] FOREIGN KEY([APPLICATION_PATH_ID])");
            sqlWritter.Append(" REFERENCES [dbo].[ENVIRONMENTS] ([ID])");
            sqlWritter.Append("         ON UPDATE CASCADE ");
            sqlWritter.Append("         ON DELETE CASCADE ");
            sqlWritter.Append("END;");

            sqlWritter.Append("ALTER TABLE [dbo].[{0}] CHECK CONSTRAINT [FK_{0}_ENVIRONMENTS];");
            //
            //-- return script
            return sqlWritter.ToString();
        }

        public static string FindColumns()
        {
            var sqlWritter = new StringBuilder();
            sqlWritter.Append("USE [Automation]");
            sqlWritter.Append("SELECT name FROM sys.all_columns WHERE object_id = (SELECT object_id FROM sys.tables WHERE name = N'{0}')");
            return sqlWritter.ToString();
        }

        public static string CreateView()
        {
            var sqlWritter = new StringBuilder();

            sqlWritter.Append("CREATE VIEW [v{0}] AS");
            sqlWritter.Append("     SELECT en.[SITE_TYPE]");
            sqlWritter.Append("		      ,en.[APPLICATION_PATH]");
            sqlWritter.Append("		      ,en.[SITE_ENV]");
            sqlWritter.Append("		      ,en.[ENV_TIMEOUT]");
            sqlWritter.Append("		      ,en.[LOGIN_PARAMS]");
            sqlWritter.Append("		      ,en.[ENVIRONMENT_LEVEL]");
            sqlWritter.Append("		      ,en.[RETRIES]");
            sqlWritter.Append("		      ,tc.*");
            sqlWritter.Append("	      FROM [ENVIRONMENTS] en");
            sqlWritter.Append("	      JOIN [{0}]          tc");
            sqlWritter.Append("	        ON en.ID = tc.[APPLICATION_PATH_ID]");
            sqlWritter.Append("		   AND en.FOR_AUTOMATION = 1");
            sqlWritter.Append("		   AND tc.FOR_AUTOMATION = 1;");

            return sqlWritter.ToString();
        }

        public static string DropTable()
        {
            var sqlWritter = new StringBuilder();

            sqlWritter.Append("USE [Automation] ");
            sqlWritter.Append("IF EXISTS (SELECT name FROM sys.tables WHERE name = N'{0}') ");
            sqlWritter.Append("BEGIN ");
            sqlWritter.Append("    DROP TABLE {0} ");
            sqlWritter.Append("END;");

            return sqlWritter.ToString();
        }

        public static string Populate(int applicationPathId, byte forAutomation, string testLevel)
        {
            var sqlWritter = new StringBuilder();

            sqlWritter.Append("INSERT INTO [dbo].[{0}] ");
            sqlWritter.Append("VALUES (");
            sqlWritter.Append("	").Append(applicationPathId).Append(" ");
            sqlWritter.Append("   ,").Append(forAutomation).Append("     ");
            sqlWritter.Append("   ,").Append(testLevel).Append("         ");
            sqlWritter.Append("    {1}); ");

            return sqlWritter.ToString();
        }
    }
}
