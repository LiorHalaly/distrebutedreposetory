﻿using Castle.Core.Logging;
using System;
using System.Diagnostics;

namespace Automation.FluentApi.InternalLogger
{
    internal class LoggerConsole : ILogger
    {
        //! MEMBERS: state
        private readonly string m_LoggerName;
        private readonly string m_ApplicationName;

        //! CONSTRUCTORS
        public LoggerConsole(string applicationName, string loggerName)
        {
            m_LoggerName = loggerName;
            m_ApplicationName = applicationName;
        }

        public bool IsDebugEnabled { get; set; }

        public bool IsErrorEnabled { get; set; } = true;

        public bool IsFatalEnabled { get; set; } = true;

        public bool IsInfoEnabled { get; set; } = true;

        public bool IsWarnEnabled { get; set; }

        public ILogger CreateChildLogger(string loggerName)
        {
            return new LoggerConsole(m_ApplicationName, loggerName);
        }

        public void Debug(string message)
        {
            if (!IsDebugEnabled) return;
            Trace.TraceInformation(message);
        }

        public void Debug(Func<string> messageFactory)
        {
            if (!IsDebugEnabled) return;
            Trace.TraceInformation(messageFactory());
        }

        public void Debug(string message, Exception exception)
        {
            if (!IsDebugEnabled) return;
            Trace.TraceInformation(message, exception);
        }

        public void DebugFormat(string format, params object[] args)
        {
            if (!IsDebugEnabled) return;
            Trace.TraceInformation(string.Format(format, args));
        }

        public void DebugFormat(Exception exception, string format, params object[] args)
        {
            if (!IsDebugEnabled) return;
            Trace.TraceInformation(string.Format(format, args), exception);
        }

        public void DebugFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void DebugFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void Error(string message)
        {
            if (!IsErrorEnabled) return;
            Trace.TraceError(message);
        }

        public void Error(Func<string> messageFactory)
        {
            if (!IsErrorEnabled) return;
            Trace.TraceError(messageFactory());
        }

        public void Error(string message, Exception exception)
        {
            if (!IsErrorEnabled) return;
            Trace.TraceError(message, exception);
        }

        public void Error(Exception exception, string methodName)
        {
            if (!IsErrorEnabled) return;
            Trace.TraceError(exception.Message, exception);
        }

        public void NoExceptionError(string message, string methodName)
        {
            if (!IsErrorEnabled) return;
            Trace.TraceError(message, methodName);
        }

        public void ErrorFormat(string format, params object[] args)
        {
            if (!IsErrorEnabled) return;
            Trace.TraceError(string.Format(format, args));
        }

        public void ErrorFormat(Exception exception, string format, params object[] args)
        {
            if (!IsErrorEnabled) return;
            Trace.TraceError(string.Format(format, args), exception);
        }

        public void ErrorFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void ErrorFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void Fatal(string message)
        {
            if (!IsFatalEnabled) return;
            Trace.TraceError(message);
        }

        public void Fatal(Func<string> messageFactory)
        {
            if (!IsFatalEnabled) return;
            Trace.TraceError(messageFactory());
        }

        public void Fatal(string message, Exception exception)
        {
            if (!IsFatalEnabled) return;
            Trace.TraceError(message, exception);
        }

        public void FatalFormat(string format, params object[] args)
        {
            if (!IsFatalEnabled) return;
            Trace.TraceError(string.Format(format, args));
        }

        public void FatalFormat(Exception exception, string format, params object[] args)
        {
            if (!IsFatalEnabled) return;
            Trace.TraceError(string.Format(format, args), exception);
        }

        public void FatalFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void FatalFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void Info(string message)
        {
            if (!IsInfoEnabled) return;
            Trace.TraceInformation(message);
        }

        public void Info(Func<string> messageFactory)
        {
            if (!IsInfoEnabled) return;
            Trace.TraceInformation(messageFactory());
        }

        public void Info(string message, Exception exception)
        {
            if (!IsInfoEnabled) return;
            Trace.TraceInformation(message, exception);
        }

        public void Info(string message, string methodName)
        {
            if (!IsInfoEnabled) return;
            Trace.TraceInformation(message, methodName);
        }

        public void InfoFormat(string format, params object[] args)
        {
            if (!IsInfoEnabled) return;
            Trace.TraceInformation(string.Format(format, args));
        }

        public void InfoFormat(Exception exception, string format, params object[] args)
        {
            if (!IsInfoEnabled) return;
            Trace.TraceInformation(string.Format(format, args), exception);
        }

        public void InfoFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void InfoFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void Warn(string message)
        {
            if (!IsWarnEnabled) return;
            Trace.TraceWarning(message);
        }

        public void Warn(string message, string methodName)
        {
            if (!IsWarnEnabled) return;
            Trace.TraceWarning(message, methodName);
        }

        public void Warn(Func<string> messageFactory)
        {
            if (!IsWarnEnabled) return;
            Trace.TraceWarning(messageFactory());
        }

        public void Warn(string message, Exception exception)
        {
            if (!IsWarnEnabled) return;
            Trace.TraceWarning(message, exception);
        }

        public void WarnFormat(string format, params object[] args)
        {
            if (!IsWarnEnabled) return;
            Trace.TraceWarning(string.Format(format, args));
        }

        public void WarnFormat(Exception exception, string format, params object[] args)
        {
            if (!IsWarnEnabled) return;
            Trace.TraceWarning(string.Format(format, args), exception);
        }

        public void WarnFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            throw new NotImplementedException();
        }

        public void WarnFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
            throw new NotImplementedException();
        }
    }
}
