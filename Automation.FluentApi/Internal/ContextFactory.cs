﻿using Castle.Core.Logging;
using System;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Automation.FluentApi.Internal
{
    internal class ContextFactory
    {
        private readonly ILogger m_Logger;
        public ContextFactory(ILogger logger)
        {
            m_Logger = logger;
        }

        public T Create<T>(params object[] args)
        {
            //-- set members flags
            const BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
            m_Logger.Debug($"binding-flags {flags} assigned");
            //
            //-- non-interface handler
            if (typeof(T).IsInterface)
                throw new ArgumentException("<T> cannot be an interface. please use the right overload to inject class into an interface");
            //
            //-- generate current class instance
            m_Logger.Debug($"type {typeof(T).AssemblyQualifiedName} is not an interface");
            return (T)Activator.CreateInstance(typeof(T), flags, null, args, null);
        }

        public T Create<T>(string classQualifiedName, params object[] args)
        {
            //-- set members flags
            const BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public;
            m_Logger.Debug($"binding-flags {flags} assigned");
            //
            //-- non-interface handler
            if (!typeof(T).IsInterface)
            {
                m_Logger.Debug($"type {typeof(T).AssemblyQualifiedName} is not an interface");
                return (T)Activator.CreateInstance(typeof(T), flags, null, args, null);
            }
            //
            //-- extract type from executing assembly (you can scan all assemblies if you want)
            var fullName = classQualifiedName.Split(';');
            var className = fullName.Last();
            var assemblyName = fullName[0];

            var type = Array
                .Find(Assembly
                .Load(assemblyName)
                .GetTypes(), t => typeof(T).IsAssignableFrom(t) && !t.IsInterface && t.Name.Equals(className, StringComparison.OrdinalIgnoreCase));
            m_Logger.Debug($"type {type.AssemblyQualifiedName} will be inject into {typeof(T).AssemblyQualifiedName}");
            //
            //-- generate current class instance
            return (T)Activator.CreateInstance(type, flags, null, args, null);
        }
    }
}