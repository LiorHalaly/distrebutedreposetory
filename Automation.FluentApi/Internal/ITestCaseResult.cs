﻿using System;
using System.Collections.Generic;

namespace Automation.FluentApi.Internal
{
    public interface ITestCaseResult
    {
        object Actual { get; set; }
        List<string> FilesList { get; set; }
        Exception Exceptions { get; set; }
    }
}
