﻿using Automation.FluentApi.Extensions;
using Automation.FluentApi.Internal;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Landscape.FluentApi.Base
{
    public abstract class TestCase : ITestCase
    {
        public virtual IDictionary<string, string> TestingParams => LoadTestingParams();
        public virtual IDictionary<string, string> Repository { get; set; }
        public virtual IDictionary<string, string> Applications { get; set; }

        public TestContext TestContext { get; set; }

        private IWebDriver m_WebDriver;
       public virtual IWebDriver WebDriver { get => m_WebDriver ?? (m_WebDriver = new ChromeDriver(@"\\tlvfs\TEST_RUNNER\WEB_DRIVERS")); private set => m_WebDriver = value; }

        private HttpClient m_HttpClient;
        public virtual HttpClient HttpClient
        { get => m_HttpClient ?? (m_HttpClient = new HttpClient { Timeout = TimeSpan.FromMinutes(10) }); private set => m_HttpClient = value; }

        public ITestCase WrappedTestCase => this;

        public virtual ITestCaseResult AutomationTest()
        {
            return new TestCaseResult { Actual = false };
        }
        
        private IDictionary<string, string> LoadTestingParams()
        {
            //-- no test-context in use
            if (TestContext == default(TestContext)) return default(Dictionary<string, string>);
            //
            // publish testing params            
            return TestContext.DataRow.ToDictionary();
        }

        public void Dispose()
        {
            if (m_WebDriver != null)
            {
                WebDriver.Dispose();
                WebDriver = null;
            }
            if (m_HttpClient != null)
            {
                HttpClient.Dispose();
                HttpClient = null;
            }           
        }
    }
}