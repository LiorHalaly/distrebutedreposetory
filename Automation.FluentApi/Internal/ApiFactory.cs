﻿using Automation.FluentApi.InternalLogger;
using Castle.Core.Logging;
using Gravity.Drivers.Selenium;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Net.Http;

namespace Automation.FluentApi.Internal
{
    public class ApiFactory : IApiFactory
    {
        //-- will hold the session web-driver which is passed by the constructor
        private static IWebDriver m_WebDriver;
        private static HttpClient m_HttpClient;
        private static string m_Application;

        //-- constructor to create web-driver
        public ApiFactory(IWebDriver webDriver)
        {
            m_WebDriver = webDriver;
        }

        //-- constructor to create http-client
        public ApiFactory(HttpClient httpClient)
        {
            m_HttpClient = httpClient;
        }

        //-- constructor to allow inheritance
        public ApiFactory() { }

        //-- public property to expose or override the session driver
        public virtual IWebDriver WebDriver => m_WebDriver;
        public virtual HttpClient HttpClient => m_HttpClient;
        public string Application { get => m_Application; private set { } }

        //-- holds the current implementation (injected) objects repository
        //-- this will be override for each implementation
        public virtual IDictionary<string, string> Repository { get; }

        //-- holds the logger for the current instance (can be override)
        public virtual ILogger Logger { get => new LoggerConsole(nameof(ApiFactory), typeof(ApiFactory).AssemblyQualifiedName); }

        //-- will allow to return the next/current page api (can be injected instead of an object)
        public T ChangeContext<T>(string clssInfo, string application, params object[] args)
        {
            //-- navigate to application
            WebDriver?.NavigateToUrl(application);
            //
            //-- set application
            m_Application = application;
            //
            //-- log entry
            Logger.Info($"navigate to {application}");
            //
            //-- return new context-interface
            return new ContextFactory(Logger).Create<T>(clssInfo, args);
        }

        //-- will allow to return the next/current page api (can be injected instead of an object)
        public T ChangeContext<T>(string application, params object[] args)
        {
            //-- navigate to application
            WebDriver?.NavigateToUrl(application);
            //
            //-- set application
            m_Application = application;
            //
            //-- log entry
            Logger.Info($"navigate to {application}");
            //
            //-- return new context-interface
            return new ContextFactory(Logger).Create<T>(args);
        }

        public T ChangeContext<T>(params object[] args)
        {
            //-- return new context-interface
            return new ContextFactory(Logger).Create<T>(args);
        }
    }
}