﻿using System;
using System.Collections.Generic;

namespace Automation.FluentApi.Internal
{
    public class TestCaseResult : ITestCaseResult
    {
        public object Actual { get; set; }
        public List<string> FilesList { get; set; }
        public Exception Exceptions { get; set ; }
    }
}
