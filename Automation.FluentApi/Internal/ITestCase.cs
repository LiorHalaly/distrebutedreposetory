﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace Automation.FluentApi.Internal
{
    public interface ITestCase : IDisposable
    {
        IWebDriver WebDriver { get; }
        HttpClient HttpClient { get; }
        TestContext TestContext { get; set; }
        IDictionary<string, string> TestingParams { get; }
        IDictionary<string, string> Repository { get; set; }
        IDictionary<string, string> Applications { get; set; }
        ITestCaseResult AutomationTest();
    }
}
