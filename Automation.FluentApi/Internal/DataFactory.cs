﻿using Automation.FluentApi.Dto;
using Automation.FluentApi.Internal.Resources;
using Automation.FluentApi.InternalLogger;
using Castle.Core.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Automation.FluentApi.Internal
{
    public class DataFactory
    {
        private readonly ILogger m_Logger;
        private IEnumerable<DataEntity> m_DataEntities;
        private string m_ConnectionString;
        private bool m_Populate;
        private  bool m_ResetSchema;

        public DataFactory()
        {
            m_Logger = new LoggerConsole(nameof(DataFactory), typeof(DataFactory).FullName);
        }

        public void Create(string connectionString, IEnumerable<DataEntity> dataEntities, bool populate = true, bool resetSchema = true)
        {
            //! pipe: step #0
            m_DataEntities = dataEntities;
            m_ConnectionString = connectionString;
            m_Populate = populate;
            m_ResetSchema = resetSchema;

            //! pipe: step #1
            NormalizeConnectionString();

            //! pipe: step #2
            CreateDatabase();

            //! pipe: step #3
            m_ConnectionString = (!Regex.IsMatch(connectionString, "Initial Catalog=([A-Za-z0-9_.]+);?", RegexOptions.IgnoreCase))
                ? connectionString += ";Initial Catalog=Automation;"
                : connectionString;

            //! pipe: step #4
            CreateSchema();

            //! pipe: step #5
            CreateTables();

            //! pipe: step #6
            if (!populate) return;
            PopulateData();
        }

        private string NormalizeConnectionString()
        {
            if (Regex.IsMatch(m_ConnectionString, "Initial Catalog=([A-Za-z0-9_.]+);?", RegexOptions.IgnoreCase))
            {
                m_ConnectionString = m_ConnectionString
                    .Replace(Regex
                    .Match(m_ConnectionString, "Initial Catalog=([A-Za-z0-9_.]+);?", RegexOptions.IgnoreCase)
                    .Value, string.Empty);
            }
            return m_ConnectionString;
        }

        private void CreateDatabase()
        {
            try
            {
                using (var sqlConnection = new SqlConnection(m_ConnectionString))
                {
                    sqlConnection.Open();
                    new SqlCommand(SqlRepository.CreateDatabase(), sqlConnection).ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                m_Logger.Error(ex.Message, ex);
                throw;
            }
        }

        private void CreateSchema()
        {
            try
            {
                using (var sqlConnection = new SqlConnection(m_ConnectionString))
                {
                    sqlConnection.Open();
                    new SqlCommand(SqlRepository.CreateSchema(), sqlConnection).ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                m_Logger.Error(ex.Message, ex);
                throw;
            }
        }

        private void CreateTables()
        {
            foreach (var dataEntity in m_DataEntities)
            {
                CreateTable(dataEntity);
                UpdateTable(dataEntity);
                CreateView(dataEntity);
            }
        }

        private void CreateTable(DataEntity dataEntity)
        {
            try
            {
                //-- Collect all the public properties of the test
                var properties = dataEntity
                    .MethodClassInstance
                    .GetType()
                    .GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                //
                //-- get method name (will be used for table name)
                var name = dataEntity.MethodName;

                // Comments -----------------------------------------------------------
                // 
                // NEW TABLE HANDLER
                // 
                // Creates "Create Table" query with the relevant new properties, and
                // add these properties as new table columns.
                //---------------------------------------------------------------------
                var columns = string.Empty;
                foreach (var property in properties)
                {
                    //-- continue condition
                    if (string.Equals(property.Name, "testcontext", StringComparison.OrdinalIgnoreCase)) continue;
                    //
                    //-- add column
                    columns += $"[{property.Name}] [nvarchar](max) NULL, ";
                }
                //
                //-- generate table
                using (var sqlConnection = new SqlConnection(m_ConnectionString))
                {
                    sqlConnection.Open();
                    //
                    //-- drop conditions
                    if (m_ResetSchema) new SqlCommand(string.Format(SqlRepository.DropTable(), name), sqlConnection).ExecuteNonQuery();
                    //
                    //-- create table
                    var command = string.Format(SqlRepository.CreateTable(), name, columns);
                    new SqlCommand(command, sqlConnection).ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                m_Logger.Error(ex.Message, ex);
                throw;
            }
        }

        private void UpdateTable(DataEntity dataEntity)
        {
            try
            {
                var name = dataEntity.MethodName;
                // Comments -----------------------------------------------------------
                // 
                // Gets all the current testClassInstance columns.
                //---------------------------------------------------------------------
                var response = new DataTable();
                using (var sqlConnection = new SqlConnection(m_ConnectionString))
                {
                    sqlConnection.Open();
                    var command = string.Format(SqlRepository.FindColumns(), name);
                    new SqlDataAdapter(new SqlCommand(command, sqlConnection)).Fill(response);
                }

                // Comments -----------------------------------------------------------
                // 
                // List to hold all the new columns which will be added/removed from
                // the table.
                //---------------------------------------------------------------------
                Tuple<List<string>, List<string>> columns = null;
                if (response.Rows.Count > 0)
                {
                    columns = GetDelta(dataEntity);
                }

                // Comments -----------------------------------------------------------
                // 
                // Remove/Add Columns:
                //     Item1: Columns to Add.
                //     Item2: Columns to Remove.
                //---------------------------------------------------------------------
                var addRemoveCommand = new StringBuilder();
                if (columns?.Item1.Count > 0)
                {
                    addRemoveCommand.AppendFormat("USE [Automation] ALTER TABLE {0} ADD ", name);
                    columns.Item1.ForEach(column => addRemoveCommand.AppendFormat("{0} NVARCHAR(MAX), ", column));

                    /* Normalize SQL Script */
                    var tmpCommand = addRemoveCommand.ToString().Substring(0, addRemoveCommand.Length - 2) + "; ";
                    addRemoveCommand.Clear();
                    addRemoveCommand.Append(tmpCommand);
                }

                if (columns?.Item2.Count > 0)
                {
                    addRemoveCommand.AppendFormat("USE [Automation] ALTER TABLE {0} DROP COLUMN ", name);
                    columns.Item2.ForEach(column => addRemoveCommand.AppendFormat("{0}, ", column));

                    /* Normalize SQL Script */
                    var tmpCommand = addRemoveCommand.ToString().Substring(0, addRemoveCommand.Length - 2) + "; ";
                    addRemoveCommand.Clear();
                    addRemoveCommand.Append(tmpCommand);
                }

                // Comments -----------------------------------------------------------
                // 
                // Execute the script.
                //---------------------------------------------------------------------
                if (addRemoveCommand.Length < 1)
                {
                    return;
                }

                using (var sqlConnection = new SqlConnection(m_ConnectionString))
                {
                    sqlConnection.Open();
                    var command = addRemoveCommand.ToString();
                    new SqlCommand(command, sqlConnection).ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                m_Logger.Error(ex.Message, ex);
                throw;
            }
        }

        private void CreateView(DataEntity dataEntity)
        {
            try
            {
                var name = dataEntity.MethodName;
                using (var sqlConnection = new SqlConnection(m_ConnectionString))
                {
                    sqlConnection.Open();
                    //
                    //-- drop view
                    var command = "USE [Automation]" + Environment.NewLine +
                                  string.Format("IF EXISTS (SELECT name FROM sys.all_views WHERE name='v{0}') BEGIN DROP VIEW [v{0}] END;", name);
                    new SqlCommand(command, sqlConnection).ExecuteNonQuery();
                    //
                    //-- create view
                    command = string.Format(SqlRepository.CreateView(), name);
                    new SqlCommand(command, sqlConnection).ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                m_Logger.Error(ex.Message, ex);
                throw;
            }
        }

        private void PopulateData()
        {
            try
            {
                foreach (var instance in m_DataEntities)
                {
                    /* Collect all the public properties of the test */
                    var properties = instance.MethodClassInstance.GetType().GetProperties(
                        BindingFlags.Public |
                        BindingFlags.Instance |
                        BindingFlags.DeclaredOnly);
                    var name = instance.MethodName;

                    /* Check if Exist */
                    const string query = "SELECT * FROM {0} WHERE [APPLICATION_PATH_ID] = 1 {1}";
                    var columns = string.Empty;

                    foreach (var property in properties)
                    {
                        //-- continue conditions
                        if (string.Equals(property.Name, "testcontext", StringComparison.OrdinalIgnoreCase)) continue;
                        //
                        //-- add column
                        columns += $"AND [{property.Name}] = '{property.GetValue(instance.MethodClassInstance, null)}' ";
                    }

                    var results = new DataTable();
                    using (var sqlConnection = new SqlConnection(m_ConnectionString))
                    {
                        sqlConnection.Open();
                        var command = string.Format(query, instance.MethodName, columns);
                        var sqlDataAdapter = new SqlDataAdapter
                        {
                            SelectCommand = new SqlCommand(command, sqlConnection)
                        };
                        sqlDataAdapter.Fill(results);
                    }
                    //
                    //-- exit conditions
                    if (results.Rows.Count != 0) return;

                    /* Insert into the table */
                    columns = string.Empty;
                    foreach (var property in properties)
                    {
                        if (string.Equals(property.Name, "testcontext", StringComparison.OrdinalIgnoreCase)) continue;
                        columns += $",'{property.GetValue(instance.MethodClassInstance, null)}'";
                    }

                    using (var sqlConnection = new SqlConnection(m_ConnectionString))
                    {
                        sqlConnection.Open();
                        var command = string
                            .Format(SqlRepository
                            .Populate(instance.ApplicationPathId, instance.ForAutomation, instance.TestLevel), name, columns);
                        new SqlCommand(command, sqlConnection).ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                m_Logger.Error(ex.Message, ex);
                throw;
            }
        }

        private Tuple<List<string>, List<string>> GetDelta(DataEntity dataEntity)
        {
            try
            {
                var name = dataEntity.MethodName;
                // Comments -----------------------------------------------------------
                // 
                // Create new instance of the main objects used in this method.
                //---------------------------------------------------------------------
                var properties = dataEntity.MethodClassInstance.GetType().GetProperties(
                    BindingFlags.Public
                    | BindingFlags.Instance
                    | BindingFlags.DeclaredOnly); /* Get the properties of the reflected object */

                // Comments ------------------------------------------------------------
                // 
                // Load the given object data entity from the database based on the
                // given connection string and data entity name.
                //---------------------------------------------------------------------
                var columns = new DataTable();
                using (var sqlConnection = new SqlConnection(m_ConnectionString))
                {
                    sqlConnection.Open();
                    var command = string.Format(
                        SqlRepository.FindColumns(), name);
                    new SqlDataAdapter(new SqlCommand(command, sqlConnection)).Fill(columns);
                }

                // Comments -----------------------------------------------------------
                // 
                // Create 2 lists to hold all the properties names and columns names
                //---------------------------------------------------------------------
                var prptiesList = new List<string>(); /* Properties List */

                /* Add columns names from data base (columnsList) */
                var columnsList = columns.Rows.Cast<DataRow>()
                    .Select(column => column["name"].ToString())
                    .Where(columnName => columnName.Any(char.IsLower) && !string.Equals(columnName, "testcontext", StringComparison.OrdinalIgnoreCase)).ToList();

                /* Add columns names from object instance (prptiesList) */
                properties.ToList().ForEach(property =>
                {
                    if (!string.Equals(property.Name, "testcontext", StringComparison.OrdinalIgnoreCase))
                    {
                        prptiesList.Add(property.Name);
                    }
                });

                // Comments -----------------------------------------------------------
                // 
                // Set the "delta" list of properties which needs to be modified
                //---------------------------------------------------------------------
                var deltaAdd = prptiesList.Except(columnsList).ToList(); /* Add    */
                var deltaRem = columnsList.Except(prptiesList).ToList(); /* Remove */

                return new Tuple<List<string>, List<string>>(deltaAdd, deltaRem);
            }
            catch (Exception ex)
            {
                m_Logger.Error(ex.Message, ex);
                throw;
            }
        }
    }
}