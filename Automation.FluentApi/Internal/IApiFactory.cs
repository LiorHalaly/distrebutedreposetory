﻿using Castle.Core.Logging;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace Automation.FluentApi.Internal
{
    public interface IApiFactory
    {
        IWebDriver WebDriver { get; }
        IDictionary<string, string> Repository { get; }
        ILogger Logger { get; }
        T ChangeContext<T>(params object[] args);
        T ChangeContext<T>(string application, params object[] args);
        T ChangeContext<T>(string clssInfo, string application, params object[] args);
    }
}
