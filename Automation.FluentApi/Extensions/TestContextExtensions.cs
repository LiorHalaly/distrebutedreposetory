﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Automation.FluentApi.Extensions
{
    public static class TestContextExtensions
    {
        public static IDictionary<string, string> ToDictionary(this DataRow dataRow)
        {
            return dataRow
                .Table
                .Columns
                .Cast<DataColumn>()
                .ToDictionary(key => key.ColumnName, str => $"{dataRow[str.ColumnName]}");
        }
    }
}
