﻿using Automation.FluentApi.Internal;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Automation.FluentApi.Extensions
{
    public static class TestCaseExtentions
    {
        public static ITestCaseResult Execute<T>(this ITestCase testCase, TestContext testContext =null, string retryColumn = null) where T : ITestCaseResult, new()
        {
            // set test-context
            testCase.TestContext = testContext;
            //
            // set initial automation sequence
            ITestCaseResult actual = new T { Actual = false, Exceptions = null };
            //
            // parse retries count
            var retries = 1;
            if (testCase.TestingParams != null)
            {
                int.TryParse(testCase.TestingParams[retryColumn], out int retriesOut);
                retries = (retriesOut == 0 || string.IsNullOrEmpty(retryColumn)) ? 1 : retriesOut;
            }
            //
            // execute automation sequence
            for (int i = 0; i < retries; i++)
            {
                actual = ExecuteSingleTestCase<T>(testCase);
                if (actual.Exceptions == null)
                {
                    break;
                }
            }
            //
            // return results
            return actual;
        }

        private static ITestCaseResult ExecuteSingleTestCase<T>(ITestCase testCase) where T : ITestCaseResult, new()
        {
            ITestCaseResult testAdapter = new T { Actual = false };
            try
            {
                testAdapter = testCase.AutomationTest();
            }
            catch (Exception ex)
            {
                testAdapter.Actual = ex;
                if (ex is AssertInconclusiveException || ex is NotImplementedException)
                {
                    throw new AssertInconclusiveException(ex.Message);
                }
                testAdapter.Exceptions = ex;
            }
            finally
            {
                testCase.Dispose();
            }
            return testAdapter;
        }
    }
}