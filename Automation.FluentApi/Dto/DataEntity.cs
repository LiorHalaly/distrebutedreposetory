﻿
namespace Automation.FluentApi.Dto
{
    public class DataEntity
    {
        public string MethodName { get; set; }

        public object MethodClassInstance { get; set; }

        public int ApplicationPathId { get; set; } = 1;

        public byte ForAutomation { get; set; } = 1;

        public string TestLevel { get; set; } = "2";
    }
}
