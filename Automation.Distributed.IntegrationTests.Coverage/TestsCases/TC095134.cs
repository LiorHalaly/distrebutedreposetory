﻿using Landscape.FluentApi.Base;
using Automation.FluentApi.Internal;
using Automation.Distributed.Products.Hotel.PageModels;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Xml.XPath;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;

namespace Automation.Distributed.IntegrationTests.Coverage.TestsCases
{
    public class TC095134 : TestCase
    {
        public override ITestCaseResult AutomationTest()
        {
            //-- get login-params
            var loginParams = JToken.Parse(TestingParams["LOGIN_PARAMS"]);
            //
            //-- set test-case actual result
            var testCase = new TestCaseResult
            {
                Actual = new ApiFactory(HttpClient)
                    .ChangeContext<SearchHotelsApi>(TestingParams["APPLICATION_PATH"])
                    .From(int.Parse(TestingParams["From"]))
                    .To(int.Parse(TestingParams["To"]))
                    .Destination(TestingParams["Destination"])
                    .UserName($"{loginParams["LoginName"]}")
                    .Password($"{loginParams["Password"]}")
                    .Version($"{loginParams["Version"]}")
                    .Perform()
                    .Entities()
            };

            //-- get all occupancy prices
            var occupPrices = ((IEnumerable<XDocument>)testCase
                .Actual)
                .Select(n => decimal.Parse(n.XPathSelectElement("//*[@occupPublishPrice]").Attribute("occupPublishPrice").Value));

            //-- assertions
            testCase.Actual = !occupPrices.Any(p => p < 1 || p > 100000);
            Assert.IsTrue((bool)testCase.Actual);

            //-- return actual
            return testCase;
        }
    }
}