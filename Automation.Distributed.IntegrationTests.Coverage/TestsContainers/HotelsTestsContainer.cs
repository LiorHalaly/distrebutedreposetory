﻿using Automation.Distributed.IntegrationTests.Coverage.TestsCases;
using Automation.FluentApi.Dto;
using Automation.FluentApi.Extensions;
using Automation.FluentApi.Internal;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Automation.Distributed.IntegrationTests.Coverage.TestsContainers
{
    [CodedUITest]
    public class HotelsTestsContainer
    {
        private const string CONNECTION_STRING = @"Data Source=TLVTSTCTRL01\SQLEXPRESS;Initial Catalog=Automation;Integrated Security=True";
        public TestContext TestContext { get; set; }

        [TestMethod, TestCategory("DataBootstrap")]
        public void HotelsTestsDataBootstrap()
        {
            var dataEntities = new List<DataEntity>();
            //
            //-- add-entities: TC095134
            for (int i = 4; i < 9; i++)
            {
                dataEntities.Add(new DataEntity
                {
                    ApplicationPathId = i,
                    ForAutomation = 1,
                    MethodClassInstance = new { Destination = "NYC", From = 30, To = 32 },
                    MethodName = nameof(TC095134),
                    TestLevel = "2"
                });
            }

            new DataFactory().Create(CONNECTION_STRING, dataEntities);
        }

        [TestMethod, DataSource("System.Data.SqlClient", CONNECTION_STRING, "vTC095134", DataAccessMethod.Sequential)]
        public void Tm095134()
        {
            //-- execute test-iterations
            var actual = new TC095134().Execute<TestCaseResult>(TestContext, "RETRIES").Actual;
            //
            //-- assert actual result
            Assert.AreEqual(true, actual);
        }
    }
}
