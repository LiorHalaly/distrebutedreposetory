﻿using Automation.Distributed.Products.Api.PageModels;
using Automation.FluentApi.Internal;
using System.Collections.Generic;

namespace Automation.Distributed.Products.Hotel.PageModels
{
    public class RoomHotel : ApiFactory, IRoom
    {
        private IEnumerable<int> m_Ages;
        private int m_NumberOfAdults;
        private int m_NumberOfChildren;

        public RoomHotel()
        {
            //-- set defaults
            m_Ages = new int[0];
            m_NumberOfAdults = 2;
            m_NumberOfChildren = 0;
        }

        public IEnumerable<int> Ages()
        {
            return m_Ages;
        }

        public IRoom Ages(IEnumerable<int> ages)
        {
            m_Ages = ages;
            return this;
        }

        public int NumberOfAdults()
        {
            return m_NumberOfAdults;
        }

        public IRoom NumberOfAdults(int numberOfAdults)
        {
            m_NumberOfAdults = numberOfAdults;
            return this;
        }

        public int NumberOfChildren()
        {
            return m_NumberOfChildren;
        }

        public IRoom NumberOfChildren(int numberOfChildren)
        {
            m_NumberOfChildren = numberOfChildren;
            return this;
        }
    }
}
