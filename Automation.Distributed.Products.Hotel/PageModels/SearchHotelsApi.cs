﻿using Automation.Distributed.Products.Api.PageModels;
using Automation.Distributed.Products.Api.PageModels.Hotels;
using Automation.Distributed.Products.Hotel.PageModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automation.Distributed.Products.Hotel.PageModels
{
    public class SearchHotelsApi : SearchSoap, ISearchHotels
    {
        //! MEMBERS: state
        private readonly List<IRoom> m_Rooms;
        internal string m_PropertyType;
        internal int m_StarLevel;
        private bool m_AvailableOnly, m_ExactDestination;
        private decimal m_MaxPrice;

        //! MEMBERS: api
        private const string m_Tourico = "http://tourico.com/webservices/hotelv3";
        private const string m_Schemas = "http://schemas.tourico.com/webservices/hotelv3";

        public SearchHotelsApi()
        {
            //-- set default values
            m_Rooms = new List<IRoom>();
            m_MaxPrice = 0;
            m_PropertyType = "Hotel";
            m_StarLevel = 0;
            m_Culture = "en_US";
        }

        public override IDictionary<string, string> Repository => new Dictionary<string, string>
        {
            ["SoapAction"] = "http://tourico.com/webservices/hotelv3/IHotelFlow/SearchHotels"
        };

        public ISearchHotels AddRoom(IRoom room)
        {
            //-- cerate new room-hotel
            var roomHotel = new RoomHotel()
                .Ages(room.Ages())
                .NumberOfChildren(room.NumberOfChildren())
                .NumberOfAdults(room.NumberOfAdults());
            //
            //-- add new room
            m_Rooms.Add(roomHotel);
            return this;
        }

        public decimal MaxPrice()
        {
            return m_MaxPrice;
        }

        public ISearchHotels MaxPrice(decimal maxPrice)
        {
            m_MaxPrice = maxPrice;
            return this;
        }

        public bool AvailableOnly()
        {
            return m_AvailableOnly;
        }

        public ISearchHotels AvailableOnly(bool availableOnly)
        {
            m_AvailableOnly = availableOnly;
            return this;
        }

        public bool ExactDestination()
        {
            return m_ExactDestination;
        }

        public ISearchHotels ExactDestination(bool exactDestination)
        {
            m_ExactDestination = exactDestination;
            return this;
        }

        public string PropertyType()
        {
            return m_PropertyType;
        }

        public ISearchHotels PropertyType(string propertyType)
        {
            m_PropertyType = propertyType;
            return this;
        }

        public int StarLevel()
        {
            return m_StarLevel;
        }

        public ISearchHotels StarLevel(int starLevel)
        {
            m_StarLevel = starLevel;
            return this;
        }

        public string DateFormat()
        {
            return m_DateFormat;
        }

        public ISearchHotels DateFormat(string dateFormat)
        {
            m_DateFormat = dateFormat;
            return this;
        }

        public string Destination()
        {
            return m_Destination;
        }

        public ISearchHotels Destination(string destination)
        {
            m_Destination = destination;
            return this;
        }

        public int From()
        {
            return m_From;
        }

        public ISearchHotels From(int from)
        {
            m_From = from;
            return this;
        }

        public int To()
        {
            return m_To;
        }

        public ISearchHotels To(int to)
        {
            m_To = to;
            return this;
        }

        public string UserName()
        {
            return m_UserName;
        }

        public ISearchHotels UserName(string userName)
        {
            m_UserName = userName;
            return this;
        }

        public string Password()
        {
            return m_Password;
        }

        public ISearchHotels Password(string password)
        {
            m_Password = password;
            return this;
        }

        public ISearchResults<object> Perform()
        {
            //-- send search-hotels request
            var httpResponseMessage = PostRequest();
            Logger.Info($"post [http://tourico.com/webservices/hotelv3/IHotelFlow/SearchHotels] request to [{Application}]");
            //
            //-- validate response
            if (!httpResponseMessage.IsSuccessStatusCode)
            {
                Logger.Warn($"post status [{httpResponseMessage.StatusCode}]; reason [{httpResponseMessage.ReasonPhrase}]");
                return new SearchResultsHotelsApi();
            }
            //
            //-- return results
            var xml = httpResponseMessage.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            return new SearchResultsHotelsApi(xml);
        }

        public override string GenerateRequestXml()
        {
            //-- initialize xml-builder
            var xmlBuilder = new StringBuilder();
            //
            //-- compose xml-request
            xmlBuilder
                .Append("<Envelope xmlns=\"").Append(m_Envelope).Append("\">")
                .Append("   <Header>")
                .Append("       <AuthenticationHeader xmlns=\"").Append(m_Authentication).Append("\">")
                .Append("           <LoginName>").Append(m_UserName).Append("</LoginName>")
                .Append("           <Password>").Append(m_Password).Append("</Password>")
                .Append("           <Culture>").Append(m_Culture).Append("</Culture>")
                .Append("           <Version>").Append(m_Version).Append("</Version>")
                .Append("       </AuthenticationHeader>")
                .Append("   </Header>")
                .Append("   <Body>")
                .Append("       <SearchHotels xmlns=\"").Append(m_Tourico).Append("\">")
                .Append("           <request>")
                .Append("               <Destination xmlns=\"").Append(m_Schemas).Append("\">").Append(m_Destination).Append("</Destination>")
                .Append("               <CheckIn xmlns=\"").Append(m_Schemas).Append("\">").Append(DateTime.Now.AddDays(m_From).ToString("yyyy-MM-dd")).Append("</CheckIn>")
                .Append("               <CheckOut xmlns=\"").Append(m_Schemas).Append("\">").Append(DateTime.Now.AddDays(m_To).ToString("yyyy-MM-dd")).Append("</CheckOut>")
                .Append(ComposeRoomInformation())
                .Append("               <MaxPrice xmlns=\"").Append(m_Schemas).Append("\">").Append(m_MaxPrice).Append("</MaxPrice>")
                .Append("               <StarLevel xmlns=\"").Append(m_Schemas).Append("\">").Append(m_StarLevel).Append("</StarLevel>")
                .Append("               <AvailableOnly xmlns=\"").Append(m_Schemas).Append("\">").Append(m_AvailableOnly ? "true" : "false").Append("</AvailableOnly>")
                .Append("               <PropertyType xmlns=\"").Append(m_Schemas).Append("\">").Append(m_PropertyType).Append("</PropertyType>")
                .Append("               <ExactDestination xmlns=\"").Append(m_Schemas).Append("\">").Append(m_ExactDestination ? "true" : "false").Append("</ExactDestination>")
                .Append("           </request>")
                .Append("           <features>")
                .Append("               <Feature/>")
                .Append("           </features>")
                .Append("       </SearchHotels>")
                .Append("   </Body>")
                .Append("</Envelope>");
            //
            //-- return composed xml
            return xmlBuilder.ToString();
        }

        private string ComposeRoomInformation()
        {
            //-- default conditions
            if (m_Rooms.Count == 0)
            {
                m_Rooms.Add(new RoomHotel());
            }
            //
            //-- iterate rooms
            var xmlBuilder = new StringBuilder().Append("<RoomsInformation xmlns=\"").Append(m_Schemas).Append("\">");
            foreach (var room in m_Rooms)
            {
                xmlBuilder
                    .Append("<RoomInfo>")
                    .Append("<AdultNum>").Append(room.NumberOfAdults()).Append("</AdultNum>")
                    .Append("<ChildNum>").Append(room.NumberOfChildren()).Append("</ChildNum>")
                    .Append("<ChildAges>");
                if (!room.Ages().Any()) xmlBuilder.Append("<ChildAge/>");
                foreach (var age in room.Ages())
                {
                    xmlBuilder.Append("<ChildAge age=\"").Append(age).Append("\"/>");
                }
                xmlBuilder
                    .Append("</ChildAges>")
                    .Append("</RoomInfo>");
            }
            xmlBuilder.Append("</RoomsInformation>");
            //
            //-- return rooms node
            return xmlBuilder.ToString();
        }

        public string Version()
        {
            return m_Version;
        }

        public ISearchHotels Version(string version)
        {
            m_Version = version;
            return this;
        }
    }
}