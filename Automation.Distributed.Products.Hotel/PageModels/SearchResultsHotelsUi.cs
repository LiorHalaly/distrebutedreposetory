﻿using System.Collections.Generic;
using OpenQA.Selenium;
using Gravity.Drivers.Selenium;
using Automation.FluentApi.Internal;
using Automation.Distributed.Products.Api.PageModels;

namespace Automation.Distributed.Products.Hotel.PageModels
{
    public class SearchResultsHotelsUi : ApiFactory, ISearchResults<object>
    {
        public override IDictionary<string, string> Repository => new Dictionary<string, string>
        {
            ["Entities"] = "//div[@itemtype='http://schema.org/Service']/div"
        };

        public object Entities()
        {
            return WebDriver.GetElements(By.XPath(Repository["Entities"]));
        }
    }
}
