﻿using System.Collections.Generic;
using Automation.Distributed.Products.Api.PageModels;
using Automation.Distributed.Products.Api.PageModels.Hotels;

namespace Automation.Distributed.Products.Hotel.PageModels
{
    public class SearchHotelsUi : SearchUi, ISearchHotels
    {
        public override IDictionary<string, string> Repository => new Dictionary<string, string>
        {
            ["Destination"] = "//input[@id='autosuggest-hotels']",
            ["AutoComplete"] = "//li[@role='option']",
            ["SearchButton"] = "//button[@id='find-hotels']",
            ["DatePickerOpen"] = "//input[@id='checkInDateInput']",
            ["DatePickerNext"] = "//span[contains(@class,'DayPickerNavigation__next')]",
            ["DatePickerDay"] = "//div[contains(@class,'CalendarMonth') and @data-visible='true']//td[contains(@class,'CalendarDay') and .='{0}']",
            ["Banners"] = "//div[@id='ZA_CAMP_A_3']"
        };

        public string DateFormat()
        {
            return m_DateFormat;
        }

        public ISearchHotels DateFormat(string dateFormat)
        {
            m_DateFormat = dateFormat;
            return this;
        }

        public string Destination()
        {
            return m_Destination;
        }

        public ISearchHotels Destination(string destination)
        {
            m_Destination = destination;
            return this;
        }

        public int From()
        {
            return m_From;
        }

        public ISearchHotels From(int from)
        {
            m_From = from;
            return this;
        }

        public int To()
        {
            return m_To;
        }

        public ISearchHotels To(int to)
        {
            m_To = to;
            return this;
        }

        public string UserName()
        {
            return m_UserName;
        }

        public ISearchHotels UserName(string userName)
        {
            m_UserName = userName;
            return this;
        }

        public string Password()
        {
            return m_Password;
        }

        public ISearchHotels Password(string password)
        {
            m_Password = password;
            return this;
        }

        public ISearchHotels AddRoom(IRoom room)
        {
            throw new System.NotImplementedException();
        }

        public decimal MaxPrice()
        {
            throw new System.NotImplementedException();
        }

        public ISearchHotels MaxPrice(decimal maxPrice)
        {
            throw new System.NotImplementedException();
        }

        public bool AvailableOnly()
        {
            throw new System.NotImplementedException();
        }

        public ISearchHotels AvailableOnly(bool availableOnly)
        {
            throw new System.NotImplementedException();
        }

        public bool ExactDestination()
        {
            throw new System.NotImplementedException();
        }

        public ISearchHotels ExactDestination(bool exactDestination)
        {
            throw new System.NotImplementedException();
        }

        public string PropertyType()
        {
            throw new System.NotImplementedException();
        }

        public ISearchHotels PropertyType(string propertyType)
        {
            throw new System.NotImplementedException();
        }

        public int StarLevel()
        {
            throw new System.NotImplementedException();
        }

        public ISearchHotels StarLevel(int starLevel)
        {
            throw new System.NotImplementedException();
        }

        public string Version()
        {
            throw new System.NotImplementedException();
        }

        public ISearchHotels Version(string version)
        {
            throw new System.NotImplementedException();
        }
    }
}