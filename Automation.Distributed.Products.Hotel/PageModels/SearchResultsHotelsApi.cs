﻿using Automation.Distributed.Products.Api.PageModels;
using Automation.FluentApi.Internal;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml.XPath;

namespace Automation.Distributed.Products.Hotel.PageModels
{
    public class SearchResultsHotelsApi : ApiFactory, ISearchResults<object>
    {
        //! MEMBERS: state
        private readonly XDocument m_XDocument;

        //! CONSTRUCTORS
        public SearchResultsHotelsApi() { }
        public SearchResultsHotelsApi(string xml)
        {
            //-- load soap response into x-document
            m_XDocument = XDocument.Parse(xml);
        }

        public object Entities()
        {
            //-- exit conditions
            if (m_XDocument == null) return new XDocument[0];
            //
            //-- wrap each hotel as x-document
            var hotelsOutput = new List<XDocument>();
            foreach (var hotel in m_XDocument.XPathSelectElements("//*[@hotelId]"))
            {
                hotelsOutput.Add(XDocument.Parse($"{hotel}"));
            }
            //
            //-- return hotels output
            return hotelsOutput;
        }
    }
}